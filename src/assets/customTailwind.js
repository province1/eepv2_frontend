// Buttons
export const tailCancelBtn  = "text-white-700 border hover:bg-red-700 hover:text-white font-bold uppercase text-sm px-6 py-3 rounded mr-1 mb-1 ease-linear transition-all duration-150";
export const tailCancelBtnDisabled = "opacity-50 cursor-not-allowed text-white-700 border font-bold uppercase text-sm px-6 py-3 rounded mr-1 mb-1 ease-linear transition-all duration-150"
export const tailConfirmBtn = "text-white bg-green-700 hover:bg-green-800 hover:text-white font-bold uppercase text-sm px-6 py-3 rounded mr-1 mb-1 ease-linear transition-all duration-150";
export const tailConfirmBtnDisabled = "opacity-50 cursor-not-allowed text-white bg-green-700 font-bold uppercase text-sm px-6 py-3 rounded mr-1 mb-1 ease-linear transition-all duration-150";

//Filter Buttons
export const tailActiveBtnFilter = "bg-green-700 hover:bg-green-800 w-fit flex items-center justify-center text-white py-0.5 px-2 rounded-sm font-normal text-base";
export const tailInactiveBtnFilter = "bg-slate-500 hover:bg-slate-600 w-fit flex items-center justify-center text-white py-0.5 px-2 rounded-sm font-normal text-base";