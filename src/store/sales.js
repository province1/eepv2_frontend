
// initial state
// shape: [{ id, quantity }]

import network_utils from './../utils/network_utils.js'

const state = () => ({
  
  sale_items: []
  
  
})

// getters
const getters = {
    sale_items: state => state.sale_items,

}

// actions
const actions = {
    
    async get_sales ({ commit }) {

        const sale_list = await network_utils.get_sales();
        
        
        const sale_list_mod = sale_list.data.map( (x) => {
            
            x.started = true
            
            if(x.start > Date.now()){
                x.started = false;
            }
            
            return x
        })
        
        console.log("Sale Data ", sale_list.data, sale_list.data.length);
        
        commit('set_sale_items', sale_list.data)
        
    },
}

// mutations
const mutations = {
    set_sale_items (state, data){
        state.sale_items = data
    },
    remove_sale(state, id) {
        let filteredData = [];
        state.sale_items.forEach(item => {
            for (let key in item) {
                if (item.sale_id != id) return filteredData.push(item)
            }
        });
       state.sale_items = filteredData;
    }
}

export default {
//namespaced: true,
  state,
  getters,
  actions,
  mutations,
  modules: {
    
  }
}