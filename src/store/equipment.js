
// initial state
// shape: [{ id, quantity }]

import network_utils from './../utils/network_utils.js'

const state = () => ({
  
  equipment_items: []
  
  
})

// getters
const getters = {
    
    equipment_items: state => state.equipment_items,

}

// actions
const actions = {
    
    async get_equipment ({ commit }, data) {
        
        console.log("equipment: ", data)
        const equipment_list = await network_utils.get_equipment(data.account);
        console.log("equipment: ", equipment_list)

        commit('set_equipment_items', equipment_list)
        
    }
    

}

// mutations
const mutations = {
    
    set_equipment_items (state, data){
        
        state.equipment_items = data
        
    }
    

}

export default {
//namespaced: true,
  state,
  getters,
  actions,
  mutations,
  modules: {
    
  }
}