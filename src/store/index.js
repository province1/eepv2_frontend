import { createStore } from 'vuex'
//import { ethers } from 'ethers'
//const eth_provider = new ethers.providers.Web3Provider(window.ethereum)
//import MapleContract from "../utils/MapleContract.json";

//const Moralis = require('moralis');
//const ethers = Moralis.web3Library

import util from '../utils/web3_utils'
import network_utils from '../utils/network_utils.js'

import equipment from  "./equipment.js"
import sales from "./sales.js"


var err_msg = ""
var error_data = {}

export default createStore({
  state: {
    
    metamask_install_status: false,
    metamask_account: {connected_status: false},
    error_message: {error: false, error_msg: ""},
    approval_result: {error: false},
    mpl_token_balance: 0,
    mpl_token_allowance: 0,
    bids: []
    
  },
  
  getters: {
    
    metamask_install_status: state => state.metamask_install_status,
    metamask_account: state => state.metamask_account,
    error_message: state => state.error_message,
    mpl_token_balance: state => state.mpl_token_balance,
    mpl_token_allowance: state => state.mpl_token_allowance,
    bids: state => state.bids,
    approval_result: state => state.approval_result
    
  },
  
  
  actions: {
    
    get_bids({ commit }){
      
      network_utils.get_bids().then( (data) => {
        
        console.log("bid data", data)
        
        commit('update_bids', data)
        
      })
      
      
    },
    
    
    check_metamask({ commit }){
      var status =  util.check_metamask_install()
      
      console.log("Metamask Install Status: ", status)
      
      commit('update_metamask_install_status', status)
      
    },
    
    connect_metamask({ commit }){
      
      
      return new Promise( (resolve, reject) =>{
        
        util.request_metamask_account().then( (result) => {
        
        
        if(result.error === false){
          
          var final_data = {connected_status: true, account: result.eth_request_result[0], chain_id: result.chain_id}
          
          
          if(result.chain_id != import.meta.env.VITE_NETWORK_ID) {
          
          
            
            err_msg = "Please make sure you are connected to the avalanche mainnet"
            error_data = {error: true, err_msg}
            
            commit('update_metamask_account', {connected_status: false})
            
            commit('publish_error', error_data)
            reject(error_data)
            
          }else {
            
            commit('update_metamask_account', final_data)
            final_data.error = false
            
            error_data = {error: false, err_msg: ""}
            
            commit('publish_error', error_data)
            
            resolve(final_data)
            
            
            
            
            
          }
          
        } else {
      
          err_msg = "You did not approve the metamask connection. Please try again or get help in the Discord server."
          error_data = {error: true, err_msg}
          
          commit('publish_error', error_data)
          reject(error_data)
        }
        
      })
        
        
      })
      
      
      
      //util.check_provider()
      
    },
    
    /*
    metamask__network_change( { commit }, data){
      
      
      commit('update_metamask_account', {connected_status: false}) 
      
    },
    */
    
    
    
    approve_max_bid({ commit }, data){
      return new Promise( (resolve, reject) =>{
        var amount = data.amount;
        
        util.approve_spend(amount.toString()).then( (result) => {
          
          
          if(result.error == true){
            
            console.log("allowance approval error: " + result)
            
            if(result.error_code == 4001){
              commit("update_approval", {error: true, error_msg: "Error: Looks like you rejected the approval request."})
              reject({error: true, error_msg: "Error: Looks like you rejected the approval request."})
            }
          } else {
            
            commit("update_approval", result)
            resolve({error: false})
            
          }
          
          
        })
      })
      
      
    },
    
    
    refresh_mpl_balance({ commit}, account){
      
      console.log("mpl balance address: ", account)
      
      util.get_mpl_balance(account).then( (result) => {
        
        console.log("mpl Balance", result)
        commit("update_mpl_balance", result)
        
      })
      
      util.get_mpl_allowance(account).then( (result) => {
        
        
        commit("update_mpl_allowance", result)
        console.log("Allowance Result: ", result)
        
      })
      
      
    },
    
  
  /*
  sign_msg(){
    
    util.generate_signed_msg('0xb23A4dC52E3f58fCcb3491398fF5Ef1C91F0F3f7', "123").then( (result) => {
        console.log("singed 2: ", result)
        
      })
    
  },
  */
  
  get_error_msg( {commit}, data ){
    
    commit('publish_error', data)
    
  }
    
    
    
  },
  
  
  mutations: {
  
    update_metamask_install_status (state, data){
        
         
      state.metamask_install_status = data
         
    },
    
    update_metamask_account (state, data) {
      
      state.metamask_account = {connected_status: data.connected_status, account: data.account, chain_id: data.chain_id}
      
    },
    
    
    publish_error(state, data){
      
      console.log("publish error data: ", data)
      
      state.error_message = data
      
    },
    
    update_approval(state, data){
      
      state.approval_result = data
      
    },
    
    update_mpl_balance(state, data){
      state.mpl_token_balance = (state,data)
    },
    
    update_mpl_allowance(state, data){
      state.mpl_token_allowance = data
    },
    
    update_bids(state, data){
      state.bids = data
    }
    
    
    
    
    
  },

  modules: {
    equipment,
    sales
  }
})
