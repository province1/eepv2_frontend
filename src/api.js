import axios from 'axios'

const provinceApi = axios.create({
  baseURL: import.meta.env.VITE_BASE_URL,
})

const salesApi = axios.create({
  baseURL: import.meta.env.VITE_SALES_URL,
})

const newSalesApi = axios.create({
  baseURL: import.meta.env.VITE_SALES_URL,
})

const createBid = axios.create({
  baseURL: import.meta.env.VITE_SALES_URL,
})

const api = { provinceApi, salesApi, newSalesApi, createBid }

export default api;