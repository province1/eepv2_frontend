import axios from 'axios'
import api from '~/api'

const base_url = 'https://api.province.gg/'
//const base_url = 'http://localhost:8081/'


async function submit_bid(bid, hash, address, message){
    
    
    var result = await axios.post(base_url+'submit_bid', {bid, hash, address, message})
    
    console.log("network utils submit bid: ", result)
    
    return result.data
    
    
}

async function submit_sale(data){
    
    var result = {};
    try {
        
        result = await api.salesApi.post('/api/sales/create', data);
        result.error = false;
        
    } catch(err){
        
        result.error = true;
        result.error_msg = err;
    }

    return result
    
    
}


async function get_bids(sale_id){
    
    var result = await api.newSalesApi.post("/api/bids/read", {sale_id})
    return result.data
}

async function get_equipment(address){
    
    console.log("get_equipment0: ",  "")
    
    var result = await axios.post( base_url + 'equipment', {address})
    
    console.log("get_equipment1: ",  result)
    
    return result.data
    
    
}

async function get_sales(){
    
    var result = await api.newSalesApi.get("/api/sales/read");
    return result
}


async function property_query(address){
     var result = await axios.post( base_url + 'query/land', {address})
     return result.data
}


async function deploy(serial, hash, address, message) {
    var result = await axios.post(base_url+'deploy', {serial, hash, address, message})
    return result.data
}

async function get_clearing_progress(serial){
    var result = await axios.post('https://api3.province.gg/'+'clearing_status', {serial})
    return result.data
}

async function get_leading_bid( sale_id ){
    return await api.newSalesApi.post("/api/bids/leading", {sale_id});
}


export default { submit_bid, submit_sale, get_bids, get_equipment, property_query, deploy, get_clearing_progress, get_sales, get_leading_bid}
