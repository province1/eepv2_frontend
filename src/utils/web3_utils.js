
var ethereum = window.ethereum

import { ethers } from 'ethers';

// import { Buffer } from 'buffer/'

const eth_provider = new ethers.providers.Web3Provider(window.ethereum)

import MapleContract from "./MapleContract.json";



function get_ethers(){
    
    return eth_provider
    
    
}



function check_metamask_install(){
    
    
    if (typeof ethereum !== 'undefined') {
        return true
    } else {
        return false
    }
    
}


async function request_metamask_account(){
    
    
    
    
    try{
        
        var eth_request_result = await ethereum.request({ method: 'eth_requestAccounts' });
        
        const chain_id_hex = await ethereum.request({ method: 'eth_chainId' });
        const chain_id_number = parseInt(chain_id_hex, 16)

        return {error: false, error_code: 0, eth_request_result, chain_id: chain_id_number}
        
      } catch(e){
        
        if(e.code == 4001){
          
          console.log("eth_requestAccounts error: ", "User rejected request")
          return {error: true, error_code: 4001}
          
        } else {
          
          console.log("unhandled eth_requestAccounts error: ", e)
          return {error: true, error_code: 1000}
          
          
        }
        
      }
      

}


async function approve_spend(mpl_nominal){
    
        var contract = new ethers.Contract(import.meta.env.VITE_MAPLE_CONTRACT_ADDRESS, 
                                            MapleContract.abi,
                                            eth_provider.getSigner()
        )
        
        
        try{    
            var result = await contract.approve(import.meta.env.VITE_TREASURY, ethers.utils.parseEther(mpl_nominal));
        
            return result
            
        } 
        
        catch(e) {
            
            if(e.code == 4001){
          
              console.log("approve spend error: ", "User rejected request")
              // return {error: true, error_code: 4001}
              return false;
            } else {
              
              console.log("unhandled eth_requestAccounts error: ", e)
              return {error: true, error_code: 1000}
              
              
            }
            
        }    
      
        
        
    

    

}

async function get_mpl_balance(address){
    
    
    var contract = new ethers.Contract(import.meta.env.VITE_MAPLE_CONTRACT_ADDRESS, 
                                            MapleContract.abi,
                                            eth_provider.getSigner())
    
    
    
    var balance = await contract.balanceOf(address);
    
    
    return ethers.utils.formatEther(balance)
                                            
}

async function get_mpl_allowance(address){
    
    
    var contract = new ethers.Contract(import.meta.env.VITE_MAPLE_CONTRACT_ADDRESS, 
                                            MapleContract.abi,
                                            eth_provider.getSigner())
                                            
    //var balance = await contract.balanceOf(address);
    
    var allowance = await contract.allowance(address, import.meta.env.VITE_MAPLE_CONTRACT_ADDRESS)
    
    
    return ethers.utils.formatEther(allowance)
                                            
}

async function generate_signed_msg(address, bid_amount){
    
    
    
    // Random string
    var random_string = (Math.random()).toString(36).substring(2);
    
    
    var msg_json = {bid: bid_amount, random_string: random_string}
    
    const msg_to_sign = JSON.stringify(msg_json);
    try {
      // const msg = `0x${Buffer.from(msg_to_sign, 'utf8').toString('hex')}`;
      const sign = await ethereum.request({
        method: 'personal_sign',
        params: [msg_to_sign, address, 'Example password'],
      });
      
      return {error: false, signature: sign, error_msg: null, message: msg_to_sign}
    } catch (err) {
      console.error(err);
      //personalSign.innerHTML = `Error: ${err.message}`;
      return {error: true, signature: null, error_msg: err, message: msg_to_sign}
    }
    
}

async function SignMessage(jsonMessage,address){
  const message = JSON.stringify(jsonMessage);
  try {
    const signature = await ethereum.request({
      method: 'personal_sign',
      params: [message, address, 'Example password'],
    });
    
    return {message,signature}
  } catch (err) {
    console.error(err);
    //personalSign.innerHTML = `Error: ${err.message}`;
    // return {error: true, signature: null, error_msg: err, message}
    return false;
  }
  
}


async function deploy(serial_number, address, new_location){
    
    
    // Random string
    var random_string = (Math.random()).toString(36).substring(2);
    var msg_json = {serial_number: serial_number, action: "deploy",random_string: random_string, new_location}
    const msg_to_sign = JSON.stringify(msg_json);
    
    try {
      const msg = `0x${Buffer.from(msg_to_sign, 'utf8').toString('hex')}`;
      const sign = await ethereum.request({
        method: 'personal_sign',
        params: [msg, address, 'Example password'],
      });
      
      return {error: false, signature: sign, error_msg: null, message: msg_to_sign}
    } catch (err) {
      console.error(err);
      //personalSign.innerHTML = `Error: ${err.message}`;
      return {error: true, signature: null, error_msg: err, message: msg_to_sign}
    }
    
    
    
}


function contract_factory(){
    
    const contract = new ethers.Contract(import.meta.env.VITE_MAPLE_CONTRACT_ADDRESS, 
                                            MapleContract.abi,
                                            eth_provider.getSigner())
                                            
    return contract
    
    
}


function approval_listener(owner, spender, value){
    
    console.log("approval_listner: ", owner, spender, value, contract_factory )
    
    
}

const increaseAllowance = async (treasury, allowance) => {
  try {
    const contract = contract_factory();
    await contract.increaseAllowance(treasury, ethers.utils.parseEther(allowance))
  } catch(err) {
    console.log(err);
  }
}

const decreaseAllowance = async (treasury, allowance) => {
  try {
    const contract = contract_factory();
    await contract.decreaseAllowance(treasury, ethers.utils.parseEther(allowance))
  } catch(err) {
    console.log(err);
  }
}

export default {decreaseAllowance, increaseAllowance, check_metamask_install, request_metamask_account, approve_spend, get_mpl_balance, get_mpl_allowance, generate_signed_msg, get_ethers, approval_listener, contract_factory, deploy, SignMessage}