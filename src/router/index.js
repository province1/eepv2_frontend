import { createRouter, createWebHistory } from 'vue-router'
import Equipment from '../views/Equipment.vue'
import Auth from '../views/Auth.vue'
import Home2 from '../views/Home2.vue'
import Land from '../views/Land.vue'
import Sales from '../views/Sales.vue'


const routes = [
  
  /*
  {
    path: '/',
    name: 'Home',
    // component: Land
    component: Home2
  },
  */
  {
    
    path: '/equipment',
    name: 'Equipment',
    component: Equipment
    
    
  },
  {
    
    path: '/land',
    name: 'Land',
    component: Land
    
    
  },
  
  {
    path: '/auth',
    name: 'Auth',
    meta: { layout: 'authLayout' },
    component: Auth
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    //component: () => import( webpackChunkName: "about"  '../views/About.vue')
  },
  {
    path: '/',
    name: 'Sales',
    component: Sales
  }
  
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router